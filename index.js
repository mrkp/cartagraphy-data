var routes = require('./routes/routes.json')
// var stops = require('./busstops.kml')

module.exports = data

function data(options) {
  if (!options) { options = {}; }
  this.routes == options.routes ? options.routes : JSON.parse(routes)
  this.stops == options.stops ? options.stops : stops
  return this
}
