var fs = require('fs'),
    xml2js = require('xml2js'),
    parser = new xml2js.Parser(),
    allRouteNumbers = [ '1', '2', '4', '7', '8', '9', '10A', '10C', '10G', '13', '14', '15', '16', '19', '21', '28', '33', '34' ],
    masterObj = {
      "SHUTTLE": []
    }
    ;

for(var index in allRouteNumbers) {
  var routeNo = allRouteNumbers[index];
  var routeIn = './routes/xml/stops route ' + routeNo + ' - 0.xml',
      routeOut = './routes/xml/stops route ' + routeNo + ' - 1.xml'
      ;
  masterObj[routeNo] = [];
  // console.log(routeIn + " — " + routeOut);
  parser.parseString(fs.readFileSync(routeIn), function (err, parsedIn) {
    parsedIn['bustime-response'].stop.forEach(function (stop) {
      masterObj[routeNo].push(parseInt(stop.stpid[0]));
      if (routeNo == 33 || routeNo == 34) {
        masterObj["SHUTTLE"].push(parseInt(stop.stpid[0]));
      }
    });
  });
  parser.parseString(fs.readFileSync(routeOut), function (err, parsedOut) {
    if (!parsedOut['bustime-response'].error) {
      parsedOut['bustime-response'].stop.forEach(function (stop) {
        masterObj[routeNo].push(parseInt(stop.stpid[0]));
        if (routeNo == 33 || routeNo == 34) {
          masterObj["SHUTTLE"].push(parseInt(stop.stpid[0]));
        }
      });
    }
  });
}

console.log(JSON.stringify(masterObj, null));

fs.writeFile('./routes/routes.json', JSON.stringify(masterObj), function (err) {
  console.log("Done!");
});
